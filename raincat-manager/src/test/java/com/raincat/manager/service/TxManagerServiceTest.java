package com.raincat.manager.service;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.raincat.common.holder.IdWorkerUtils;
import com.raincat.common.netty.bean.TxTransactionGroup;
import com.raincat.common.netty.bean.TxTransactionItem;


/**
 * <p>Description: .</p>
 * <p>Company: 深圳市旺生活互联网科技有限公司</p>
 * <p>Copyright: 2015-2017 happylifeplat.com All Rights Reserved</p>
 *
 * @author yu.xiao@happylifeplat.com
 * @version 1.0
 * @date 2017/8/8 14:13
 * @since JDK 1.8
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TxManagerServiceTest {


    @Autowired
    private TxManagerService txManagerService;


    @Test
    public void saveTxTransactionGroup() {

        TxTransactionGroup txTransactionGroup = new TxTransactionGroup();
        txTransactionGroup.setId(IdWorkerUtils.getInstance().createGroupId());

        TxTransactionItem item = new TxTransactionItem();
        item.setStatus(5);
        item.setTaskKey(IdWorkerUtils.getInstance().createTaskKey());

        txTransactionGroup.setItemList(Collections.singletonList(item));

        txManagerService.saveTxTransactionGroup(txTransactionGroup);
    }

    @Test
    public void addTxTransaction() {
    }

    @Test
    public void listByTxGroupId() {
        final List<TxTransactionItem> txTransactionItems = txManagerService.listByTxGroupId("872308019");
        Assert.assertNotNull(txTransactionItems);
    }

    @Test
    public void removeRedisByTxGroupId() {
    }

    @Test
    public void updateTxTransactionItemStatus() {
    }

    @Test
    public void findTxTransactionGroupStatus() {
    }

    @Test
    public void removeCommitTxGroup() {
    }

}