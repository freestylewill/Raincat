/*
 *
 * Copyright 2017-2018 549477611@qq.com(xiaoyu)
 *
 * This copyrighted material is made available to anyone wishing to use, modify,
 * copy, or redistribute it subject to the terms and conditions of the GNU
 * Lesser General Public License, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution; if not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.raincat.common.netty.serizlize.protostuff;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.raincat.common.netty.MessageCodecService;

import io.netty.buffer.ByteBuf;

/**
 * ProtostuffCodecServiceImpl.
 *
 * @author xiaoyu
 */
public class ProtostuffCodecServiceImpl implements MessageCodecService {

    private final ProtostuffSerializePool pool = ProtostuffSerializePool.getProtostuffPoolInstance();

    @Override
    public void encode(final ByteBuf out, final Object message) throws IOException {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            ProtostuffSerialize protostuffSerialization = pool.borrow();
            protostuffSerialization.serialize(byteArrayOutputStream, message);
            byte[] body = byteArrayOutputStream.toByteArray();
            int dataLength = body.length;
            out.writeInt(dataLength);
            out.writeBytes(body);
            pool.restore(protostuffSerialization);
        }
    }

    @Override
    public Object decode(final byte[] body) {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body)) {
            ProtostuffSerialize protostuffSerialization = pool.borrow();
            Object object = protostuffSerialization.deserialize(byteArrayInputStream);
            pool.restore(protostuffSerialization);
            return object;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

